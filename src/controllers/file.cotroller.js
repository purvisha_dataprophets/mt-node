const File = require('../models/fileUploadModel');
const mongoose = require('mongoose');

module.exports = {
  upload :function(req,permitedUser){
    const { path, mimetype } = req.file;
    console.log("req",mimetype)
      return File({
           uploadedBy: req.user.id,
          fileName: req.file.originalname,
          fileMimeType: mimetype,
          filePath: './' + req.file.path, 
          permittedUsers: permitedUser ? permitedUser : null,
      })
  },
   findFile :function(id) {
    return File.findOne({_id: mongoose.Types.ObjectId(id)})
},
 findAllFile :function(){
    return File.find({})
}
};
